var express = require('express');
var app = express();

app.use(express.static('dist'));

app.get('/wrapper', function( req, res ){
	res.sendfile( __dirname + 'views/wrapper.html' )
});

app.get('/preprocessed', function( req, res ){
	res.sendfile( __dirname + 'views/preprocessed.html' )
});