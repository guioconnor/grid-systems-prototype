'use strict';

var gulp        = require('gulp');
var less        = require('gulp-less');
var path        = require('path');
var postcss     = require('gulp-postcss');
var minifyCss   = require('gulp-minify-css');
var cssbeautify = require('gulp-cssbeautify');

var processors = [
        require('postcss-mixins'),
        require('postcss-simple-vars'),
        require('postcss-nested'),
        function(css) {
            // sans-serif fallback
            css.eachDecl('font-family', function(decl) {
                decl.value = decl.value + ', sans-serif';
            });
        },
        require("css-mqpacker")(),
        require('autoprefixer')({ browsers: ['last 2 versions', '> 2%'] })
    ];



// gulp.task('wrapper', function() {
//     return gulp.src('src/wrapper/**/*.less')
//         .pipe(less({
//             paths: [ path.join(__dirname, 'less', 'includes') ]
//         }))
//         .pipe(gulp.dest('./dist/css/wrapper'));
// });

gulp.task('optmisecss', function() {
  return gulp.src('src/cssoutput/**/*.css')
    .pipe(postcss(processors))
    .pipe(minifyCss())
    .pipe(cssbeautify())
    .pipe(gulp.dest('dist/css/'));
});

gulp.task('preprocessor', function(){
    return gulp.src('src/preprocessor/**/*.less')
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest('./src/cssoutput'));
});

gulp.task('globalcss', function(){
    return gulp.src('src/global/**/*.less')
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('watch', function(){
    gulp.watch('src/preprocessor/**/*.less', ['preprocessor']);
    gulp.watch('src/global/**/*.less',       ['globalcss']);
    gulp.watch('src/cssoutput/**/*.css',     ['optmisecss']);
});

gulp.task('default', ['preprocessor', 'globalcss', 'watch']);